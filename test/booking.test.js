import chai from 'chai'
import chaiHttp from 'chai-http'
import api from '../index.js'
import moment from 'moment'
import { v4 as uuidv4 } from 'uuid'

chai.use(chaiHttp)

describe('Bookings', function () {
  it('GET /bookings should return an empty array', function (done) {
    chai.request(api)
      .get('/bookings')
      .end((err, res) => {
        chai.expect(res.statusCode).to.equal(200)
        chai.expect(res.body).to.deep.equal({ data: [] })
        done()
      })
  })

  it('POST /bookings', function (done) {
    const booking = {
      id: uuidv4(),
      rentDate: moment().add(1, 'days').format('YYYY-MM-DD'),
      returnDate: moment().add(2, 'days').format('YYYY-MM-DD'),
      book: '9876543210',
      user: '87a6d204-b530-4956-a71e-5ca09406c67b'
    }
    chai.request(api)
      .post('/bookings')
      .send(booking)
      .end((_, res) => {
        chai.expect(res.statusCode).to.equal(201)
        chai.expect(res.body).to.deep.equal({
          data: booking
        })
        done()
      })
  })

  it('POST /bookings with invalid dates should return a 400 error', function (done) {
    const invalidBooking = {
      id: uuidv4(),
      rentDate: '2023-05-10',
      returnDate: '2023-05-09',
      book: '9876543210',
      user: '87a6d204-b530-4956-a71e-5ca09406c67b'
    }
    chai.request(api)
      .post('/bookings')
      .send(invalidBooking)
      .end((_, res) => {
        chai.expect(res.statusCode).to.equal(400)
        chai.expect(res.body).to.deep.equal({
          error: {
            message: 'Rent date must be before return date',
          },
        })
        done()
      })
  }) 

  it('POST /bookings with non-existing user should return a 400 error', function (done) {
    const invalidBooking = {
      id: uuidv4(),
      rentDate: moment().add(1, 'days').format('YYYY-MM-DD'),
      returnDate: moment().add(2, 'days').format('YYYY-MM-DD'),
      book: '9876543210',
      user: '666'
    }
    chai.request(api)
      .post('/bookings')
      .send(invalidBooking)
      .end((_, res) => {
        chai.expect(res.statusCode).to.equal(400)
        chai.expect(res.body).to.deep.equal({
          error: {
            message: 'User not found',
          },
        })
        done()
      })
  })

  it('POST /bookings should not allow booking a book that is already rented', function (done) {
    // Create a booking
    const booking1 = {
      id: uuidv4(),
      rentDate: '2023-06-10',
      returnDate: '2023-06-15',
      book: '1234567890',
      user: '87a6d204-b530-4956-a71e-5ca09406c67b'
    }
    chai.request(api)
      .post('/bookings')
      .send(booking1)
      .end(function (error, response) {
        chai.expect(response.statusCode).to.equal(201)
  
        // Attempt to create another booking with the same book and overlapping dates
        const booking2 = {
          id: uuidv4(),
          rentDate: '2023-06-12',
          returnDate: '2023-06-17',
          book: '1234567890',
          user: '7c850a21-2935-42ca-85d8-e3db49ddc0fb'
        }
        chai.request(api)
          .post('/bookings')
          .send(booking2)
          .end(function (error, response) {
            chai.expect(response.statusCode).to.equal(400)
            chai.expect(response.body).to.deep.equal({
              error: {
                message: 'The book is already rented during the requested period.',
              },
            })
            done()
          })
      })
  })
  
it('PUT should update a booking', function (done) {
  // Create a new booking
  const bookingId = uuidv4()
  const newBooking = {
    id: bookingId,
    user: '87a6d204-b530-4956-a71e-5ca09406c67b',
    book: '9876543210',
    rentDate: '2022-06-15',
    returnDate: '2022-06-20'
  }
  chai.request(api)
    .post('/bookings')
    .send(newBooking)
    .end(function (error, response) {
      chai.expect(response.statusCode).to.equal(201)

      // Update the booking
      const updatedBooking = {
        user: '7c850a21-2935-42ca-85d8-e3db49ddc0fb',
        book: '2',
        rentDate: '2022-06-20',
        returnDate: '2022-06-25'
      }
      chai.request(api)
        .put(`/bookings/${bookingId}`)
        .send(updatedBooking)
        .end(function (error, response) {
          chai.expect(response.statusCode).to.equal(200)
          chai.expect(response.body.data).to.deep.equal({
            ...newBooking,
            ...updatedBooking,
            id: bookingId
          })
          done()
        })
    })
  })

  it('PUT /bookings should not allow updating a booking with a book that is already rented', function (done) {
    // Create two bookings with different books
    const booking1 = {
      id: uuidv4(),
      rentDate: '2023-06-10',
      returnDate: '2023-06-15',
      book: '1234567890',
      user: '87a6d204-b530-4956-a71e-5ca09406c67b'
    }
    const booking2 = {
      id: uuidv4(),
      rentDate: '2023-06-12',
      returnDate: '2023-06-17',
      book: '0987654321',
      user: '7c850a21-2935-42ca-85d8-e3db49ddc0fb'
    }
    chai.request(api)
      .post('/bookings')
      .send(booking1)
      .end(function (error, response) {
        chai.expect(response.statusCode).to.equal(400)
  
        chai.request(api)
          .post('/bookings')
          .send(booking2)
          .end(function (error, response) {
            chai.expect(response.statusCode).to.equal(201)
  
            // Attempt to update booking2 with the same book as booking1 and overlapping dates
            const updatedBooking = {
              rentDate: '2023-06-10',
              returnDate: '2023-06-15',
              book: '1234567890',
              user: '7c850a21-2935-42ca-85d8-e3db49ddc0fb'
            }
            chai.request(api)
              .put(`/bookings/${booking2.id}`)
              .send(updatedBooking)
              .end(function (error, response) {
                chai.expect(response.statusCode).to.equal(400)
                chai.expect(response.body).to.deep.equal({
                  error: {
                    message: 'The book is already rented during the requested period.',
                  },
                })
                done()
              })
          })
      })
  })
  
  

})
