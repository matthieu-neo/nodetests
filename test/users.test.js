import chai from 'chai'
import chaiHttp from 'chai-http'
import api from '../index.js'
import { v4 as uuidv4 } from 'uuid'

// IMPORTANT : For Mocha working, always use function () {}
// (never () => {})

chai.use(chaiHttp)

describe('Users', function () {
  it('GET /users : Get a user', function (done){
    chai.request(api)
    .get('/users')
    .end((_,res) => {
      chai.expect(res.statusCode).to.equal(200)
      chai.expect(res.body).to.deep.equal({
        data : [{
          id: '87a6d204-b530-4956-a71e-5ca09406c67b',
          lastName: 'Dark',
          firstName: 'Vador',
          birthDate: '1969-12-25',
          address: 'L/étoile de la mort',
          phone: "+33111111111",
          email: 'darkvador@empire.com'
        },
        {
          id: '7c850a21-2935-42ca-85d8-e3db49ddc0fb',
          lastName: 'Poutine',
          firstName: 'Vladimir',
          birthDate: '1952-07-10',
          address: 'Moscou',
          phone: '0033754321098',
          email: 'vladimou@popov.com'
        }]
      })
      done()
    })
   })

   it('POST /users', function(done){
    const user = {
      id: uuidv4(),
      lastName: 'Norris',
      firstName: 'Chuck',
      birthDate: '1940-03-10',
      address: 'Etats-Unis',
      phone: '+33160203046',
      email: 'chuckNorris@god.com'
    }
    chai.request(api)
    .post('/users')
    .send(user)
    .end((_,res) => {
      chai.expect(res.statusCode).to.equal(201)
      chai.expect(res.body).to.deep.equal({
        data:user
      })
      done()
    })
   })

   it('POST /users should return 400 if birthDate is not in format YYYY-MM-DD', function(done){
    const user = {
      id: uuidv4(),
      lastName: 'Norris',
      firstName: 'Chuck',
      birthDate: '10-03-1940', // date in incorrect format
      address: 'Etats-Unis',
      phone: '0601020304',
      email: 'chuckNorris@god.com'
    }
    chai.request(api)
    .post('/users')
    .send(user)
    .end((_,res) => {
      chai.expect(res.statusCode).to.equal(400)
      chai.expect(res.body).to.deep.equal({
        error: {
          message: "Invalid birth date format. Use 'YYYY-MM-DD' format.",
        }
      })
      done()
    })
  })  

  it('POST /users should not allow creating a user with an invalid phone number format', function(done){
    const user = {
      id: uuidv4(),
      lastName: 'Norris',
      firstName: 'Chuck',
      birthDate: '1940-03-10',
      address: 'Etats-Unis',
      phone: '0602030', // Format de téléphone invalide
      email: 'chuckNorris@god.com'
    }
    chai.request(api)
    .post('/users')
    .send(user)
    .end((_,res) => {
      chai.expect(res.statusCode).to.equal(400)
      chai.expect(res.body).to.deep.equal({
        error: {
          message: "Invalid phone number. The phone number must start with '+33', '0033' or '0', followed by exactly 9 digits.",
        },
      })
      done()
    })
  })
  

   it('DELETE /users/:id', function(done){
    const userId = '87a6d204-b530-4956-a71e-5ca09406c67b';
  
    chai.request(api)
    .delete('/users/' + userId)
    .end((_,res) => {
      chai.expect(res.statusCode).to.be.oneOf([200, 404])
      if (res.statusCode === 200) {
        chai.expect(res.body).to.deep.equal({
          message: 'L\'utilisateur a été supprimé avec succès',
          data: {
            id: '87a6d204-b530-4956-a71e-5ca09406c67b',
            lastName: 'Dark',
            firstName: 'Vador',
            birthDate: '1969-12-25',
            address: 'L/étoile de la mort',
            phone: '+33111111111',
            email: 'darkvador@empire.com'
          }
        })
      } else {
        chai.expect(res.body).to.deep.equal({
          message: 'L\'utilisateur n\'a pas été trouvé'
        })
      }
      done()
    })
  })

  it('PUT /users/:id', function(done){
    const user = {
      id: '7c850a21-2935-42ca-85d8-e3db49ddc0fb',
      lastName: 'Fett',
      firstName: 'Boba',
      birthDate: '1970-12-25',
      address: 'Tatooine',
      phone: '+33698765432',
      email: 'bobaFett@chasseur.com'
    }
    chai.request(api)
    .put('/users/7c850a21-2935-42ca-85d8-e3db49ddc0fb')
    .send(user)
    .end((_,res) => {
      chai.expect(res.statusCode).to.equal(200)
      chai.expect(res.body).to.deep.equal({
        data:user
      })
      done()
    })
   })

   it('PUT /users/:id should not allow updating birthdate with wrong format', function(done){
    const user = {
      id: '7c850a21-2935-42ca-85d8-e3db49ddc0fb',
      lastName: 'Fett',
      firstName: 'Boba',
      birthDate: '25/12/1970',
      address: 'Tatooine',
      phone: '0601020304',
      email: 'bobaFett@chasseur.com'
    }
    chai.request(api)
    .put('/users/7c850a21-2935-42ca-85d8-e3db49ddc0fb')
    .send(user)
    .end((_,res) => {
      chai.expect(res.statusCode).to.equal(400)
      chai.expect(res.body).to.deep.equal({
        error: {
          message: "Invalid birth date format. Use 'YYYY-MM-DD' format.",
        },
      })
      done()
    })
   })

   it('PUT /users/:id should not allow updating a user with an invalid phone number format', function(done) {
    const updatedUser = {
      lastName: 'Vader',
      firstName: 'Darth',
      birthDate: '1980-05-04',
      address: 'Death Star',
      phone: '060203', // invalid phone number format
      email: 'darthVader@empire.com'
    }
  
    chai.request(api)
      .put('/users/87a6d204-b530-4956-a71e-5ca09406c67b')
      .send(updatedUser)
      .end(function (error, response) {
        chai.expect(response.statusCode).to.equal(400)
        chai.expect(response.body).to.deep.equal({
          error: {
            message: 'Invalid phone number. The phone number must start with \'+33\', \'0033\' or \'0\', followed by exactly 9 digits.',
          },
        })
        done()
      })
  })

})