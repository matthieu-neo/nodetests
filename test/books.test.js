import chai from 'chai'
import chaiHttp from 'chai-http'
import api from '../index.js'

// IMPORTANT : For Mocha working, always use function () {}
// (never () => {})

chai.use(chaiHttp)

describe('Books', function () {

 it('GET /books : should return a success response', function (done){
  chai.request(api)
  .get('/books')
  .end((_,res) => {
    chai.expect(res.statusCode).to.equal(200)
    chai.expect(res.body).to.deep.equal({
      data : [{
        isbn13: '9782744005084',
        title: 'UML et C++',
        authors: 'Richard C. Lee, William M. Tepfenhart',
        editor: 'CampusPress',
        langCode: 'FR',
        price: 29.95
      },
      {
        isbn13: '9782746035966',
        title: 'Cree su primer sitio web con dreamweaver 8',
        authors: 'B.A. GUERIN',
        editor: 'ENI',
        langCode: 'ES',
        price: 10.02
      }]
    })
    done()
  })
 })

 it('POST /books', function(done){
  const book = {
    isbn13: '618168516581',
    title: 'Le sexe pour les puceaux',
    authors: 'HERBAYS Kyllian',
    editor: 'SexeEdit',
    langCode: 'FR',
    price: 69
  }
  chai.request(api)
  .post('/books')
  .send(book)
  .end((_,res) => {
    chai.expect(res.statusCode).to.equal(201)
    chai.expect(res.body).to.deep.equal({
      data:book
    })
    done()
  })
 })

 it('PUT /books/:id', function(done){
  const book = {
    isbn13: '9782746035966',
    title: 'Foo-Bar',
    authors: 'Polo',
    editor: 'ENI',
    langCode: 'FR',
    price: 69
  }
  chai.request(api)
  .put('/books/9782746035966')
  .send(book)
  .end((_,res) => {
    chai.expect(res.statusCode).to.equal(200)
    chai.expect(res.body).to.deep.equal({
      data:book
    })
    done()
  })
 })

 it('PUT /books/:id : should not exist book', function(done){
  const book = {
    isbn13: '9782746035966',
    title: 'Foo-Bar',
    authors: 'Polo',
    editor: 'ENI',
    langCode: 'FR',
    price: 69
  }
  const fakeId = '0646464544'

  chai.request(api)
  .put(`/books/${fakeId}`)
  .send(book)
  .end((_,res) => {
    chai.expect(res.statusCode).to.equal(404)
    chai.expect(res.body).to.deep.equal({
      error: {
        message: `The book this id : ${fakeId} is not found`
      }
    })
    done()
  })
 })
 
 it('DELETE /books/:id', function(done){
  const bookId = '618168516581';

  chai.request(api)
  .delete('/books/' + bookId)
  .end((_,res) => {
    chai.expect(res.statusCode).to.be.oneOf([200, 404])
    if (res.statusCode === 200) {
      chai.expect(res.body).to.deep.equal({
        message: 'Le livre a été supprimé avec succès',
        data: {
          isbn13: '618168516581',
          title: 'Le sexe pour les puceaux',
          authors: 'HERBAYS Kyllian',
          editor: 'SexeEdit',
          langCode: 'FR',
          price: 69
        }
      })
    } else {
      chai.expect(res.body).to.deep.equal({
        message: 'Le livre n\'a pas été trouvé'
      })
    }
    done()
  })
})

})
