export default (User) => {

  const users = [
    new User('87a6d204-b530-4956-a71e-5ca09406c67b', 'Dark', 'Vador', '1969-12-25', 'L/étoile de la mort','+33111111111','darkvador@empire.com'),
    new User('7c850a21-2935-42ca-85d8-e3db49ddc0fb', 'Poutine', 'Vladimir', '1952-07-10', 'Moscou','0033754321098','vladimou@popov.com')
  ]

  const listUsers = () => {
    return users;
  }

  const getUserById = (id) => {
    return users.find((user) => user.id === id);
  };
  

  const createUser = (user) => {
    users.push(user)
    return user
  }

  const deleteUser = (id) => {
    const index = users.findIndex((user) => user.id === id)
    if (index !== -1) {
      return users.splice(index, 1)
    }
    return null
  }

  const updateUser = (userId, userData) => {
    let foundUserIndex = -1
    users.forEach((user,index) => {
      if(user.id === userId){
        foundUserIndex = index
      }
    })
    if(foundUserIndex > -1){
      users[foundUserIndex] = userData
      return users[foundUserIndex]
    }
  
    return null
  }

  return {
    listUsers,
    createUser,
    deleteUser,
    updateUser,
    getUserById
  }
}