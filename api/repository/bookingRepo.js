import moment from 'moment';

export default () => {
  const bookings = []

  function getAllBookings() {
    return bookings
  }

  const createBooking = (booking) => {
    bookings.push(booking)
    return booking
  }

  const updateBooking = (id, booking) => {
    const index = bookings.findIndex((b) => b.id === id)
    if (index !== -1) {
      bookings[index] = { ...booking, id }
      return bookings[index]
    }
    return null
  }

  const isBookAlreadyRented = (book, rentDate, returnDate) => {
    return bookings.some((booking) => {
      return booking.book === book &&
        (
          (moment(rentDate).isBetween(booking.rentDate, booking.returnDate, undefined, '[)')) ||
          (moment(returnDate).isBetween(booking.rentDate, booking.returnDate, undefined, '()')) ||
          (moment(booking.rentDate).isBetween(rentDate, returnDate, undefined, '[)')) ||
          (moment(booking.returnDate).isBetween(rentDate, returnDate, undefined, '()'))
        );
    });
  };
  

  return {
    getAllBookings,
    createBooking,
    updateBooking,
    isBookAlreadyRented
  }
}