
export default (Book) => {

  const books = [
    new Book('9782744005084', 'UML et C++', 'Richard C. Lee, William M. Tepfenhart', 'CampusPress', 'FR', 29.95),
    new Book('9782746035966', 'Cree su primer sitio web con dreamweaver 8', 'B.A. GUERIN', 'ENI', 'ES', 10.02)
  ]
  
  const listBooks = () => {
    return books;
  }

  const createBook = (book) => {
    books.push(book)
    return book
  }

  const updateBook = (bookId, bookData) => {
    let foundBookIndex = -1
    books.forEach((book,index) => {
      if(book.isbn13 === bookId){
        foundBookIndex = index
      }
    })
    
    if(foundBookIndex > -1){
      books[foundBookIndex] = bookData
      return books[foundBookIndex]
    }

    return null
  }

  const deleteBook = (id) => {
    const index = books.findIndex(book => book.id === id)
    if (index !== -1) {
      const deletedBook = books.splice(index, 1)
      return deletedBook
    } else {
      return null
    }
  }

  return {
    listBooks,
    createBook,
    updateBook,
    deleteBook
  }
}
