import bookControler from './bookControler.js'
import userControler from './userControler.js'
import bookingControler from './bookingControler.js'
import statusCheck from './statusCheck.js'

export default (repository) => {
  return{
    statusCheck,
    bookControler : bookControler(repository),
    userControler : userControler(repository),
    bookingControler : bookingControler(repository)
  }
}
