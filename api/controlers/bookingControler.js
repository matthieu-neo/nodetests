import moment from 'moment';

export default (repository) => {
  const bookingRepo = repository.bookingRepo
  const userRepo = repository.userRepo;

  function isValidDateFormat(date) {
    const regex = /^\d{4}-\d{2}-\d{2}$/;
    return regex.test(date);
  }
  
  const getAllBookings = (req, res) => {
    const bookings = bookingRepo.getAllBookings()
    res.send({
      data: bookings
    })
  }

  const createBooking = (req, res) => {
    try {
      const { rentDate, returnDate, user } = req.body;

       // Vérifiez si l'utilisateur existe
      const existingUser = userRepo.getUserById(user);

      if (!existingUser) {
        return res.status(400).send({
          error: {
            message: "User not found",
          },
        });
      }

      if (bookingRepo.isBookAlreadyRented(req.body.book, req.body.rentDate, req.body.returnDate)) {
        return res.status(400).send({
          error: {
            message: 'The book is already rented during the requested period.',
          },
        });
      }
    
      if (!isValidDateFormat(rentDate) || !isValidDateFormat(returnDate)) {
        return res.status(400).send({
          error: {
            message: "Invalid date format. Use 'YYYY-MM-DD' format.",
          },
        });
      }
  
      if (moment(rentDate).isSameOrAfter(moment(returnDate))) {
        return res.status(400).send({
          error: {
            message: 'Rent date must be before return date',
          },
        })
      }
    
      const booking = bookingRepo.createBooking(req.body);
      res.status(201).send({
        data: booking,
      });
    } catch (error) {
      console.error(error);
      res.status(500).send({
        error: {
          message: 'Internal server error',
        },
      });
    }
  };
  

  const updateBooking = (req, res) => {
    const { id } = req.params;
    const { rentDate, returnDate, user } = req.body;

    if (bookingRepo.isBookAlreadyRented(req.body.book, req.body.rentDate, req.body.returnDate)) {
      return res.status(400).send({
        error: {
          message: 'The book is already rented during the requested period.',
        },
      });
    }
  
    // Vérifiez si l'utilisateur existe
    const existingUser = userRepo.getUserById(user);
  
    if (!existingUser) {
      console.log("Error: User not found");
      return res.status(400).send({
        error: {
          message: "User not found",
        },
      });
    }
  
    if (!isValidDateFormat(rentDate) || !isValidDateFormat(returnDate)) {
      console.log("Error: Invalid date format");
      return res.status(400).send({
        error: {
          message: "Invalid date format. Use 'YYYY-MM-DD' format.",
        },
      });
    }
  
    if (moment(rentDate).isSameOrAfter(moment(returnDate))) {
      console.log("Error: Rent date must be before return date");
      return res.status(400).send({
        error: {
          message: 'Rent date must be before return date',
        },
      });
    }
  
    const updatedBooking = bookingRepo.updateBooking(id, req.body);
    res.status(200).send({
      data: updatedBooking,
    });
  };
  
  
  return {
    getAllBookings,
    createBooking,
    updateBooking,
    isValidDateFormat
  }
}