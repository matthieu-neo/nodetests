
export default (controlers, app) => {
  app.get('/statusCheck', controlers.statusCheck.getStatus)
  app.get('/books', controlers.bookControler.listBooks)
  app.post('/books', controlers.bookControler.createBook)
  app.put('/books/:id', controlers.bookControler.updateBook)
  app.delete('/books/:id', controlers.bookControler.deleteBook)
  app.get('/users', controlers.userControler.listUsers)
  app.post('/users', controlers.userControler.createUser)
  app.delete('/users/:id', controlers.userControler.deleteUser)
  app.put('/users/:id', controlers.userControler.updateUser)
  app.get('/bookings', controlers.bookingControler.getAllBookings)
  app.post('/bookings', controlers.bookingControler. createBooking)
  // app.delete('/bookings/:id', controlers.bookingControler.deleteBooking)
  app.put('/bookings/:id', controlers.bookingControler.updateBooking)
}
